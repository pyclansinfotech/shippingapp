from django.contrib.auth.models import User
from models import *
from rest_framework import serializers

# Serializers define the API representation of Users
class UsersSerializer(serializers.ModelSerializer):
    class Meta:
    	queryset = Users.objects.all()
        model = Users
        fields = ('user_id','name', 'company', 'email', 'password', 'role')
        allowed_methods = ('get', 'post', 'put', 'delete','patch')

# Serializers define the API representation of Ships
class ShipsSerializer(serializers.ModelSerializer):
    class Meta:
    	queryset = Ship.objects.all()
        model = Ship
        fields = ('id','name', 'type', 'fleet_manager', 'superintendent','company_id')
        allowed_methods = ('get', 'post', 'put', 'delete','patch')

# Serializers define the API representation of Questions
class QuestionsSerializer(serializers.ModelSerializer):
    class Meta:
    	queryset = Template_Questions.objects.all()
        model = Template_Questions
        fields = ('tq_id','template_question', 'tc_name')
        allowed_methods = ('get', 'post', 'put', 'delete','patch')

# Serializers define the API representation of Templates
class TemplatesSerializer(serializers.ModelSerializer):
    class Meta:
    	queryset = Question_categories.objects.all()
        model = Question_categories
        fields = ('qc_id','ship_type','category_name', 'company')
        allowed_methods = ('get', 'post', 'put', 'delete','patch')