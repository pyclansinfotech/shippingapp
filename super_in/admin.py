from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from models import Ship


class ShipAdmin(admin.ModelAdmin):
      list_display    = ['type','fleet_manager','superintendent']

admin.site.register(Ship, ShipAdmin)

