from tastypie.resources import ModelResource
from tastypie.constants import ALL
from super_in.models import *


class UsersResource(ModelResource):
	class Meta:
		queryset = Users.objects.all()
		resource_name = 'user'
		allowed_methods = ('get', 'post', 'put', 'delete','patch')
		


class ShipsResource(ModelResource):
	class Meta:
		queryset = Ship.objects.all()
		resource_name = 'ship'
		allowed_methods = ('get', 'post', 'put', 'delete','patch')


class QuestionsResource(ModelResource):
	class Meta:
		queryset = Template_Questions.objects.all()
		resource_name = 'template'
		allowed_methods = ('get', 'post', 'put', 'delete','patch')


class TemplatesResource(ModelResource):
	class Meta:
		queryset = Question_categories.objects.all()
		resource_name = 'questions'
		allowed_methods = ('get', 'post', 'put', 'delete','patch')
		