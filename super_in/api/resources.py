from tastypie.resources import ModelResource
from super_in.models import *


class CompanyResource(ModelResource):
    class Meta:
        queryset = Company.objects.all()
        allowed_methods = ['get']
