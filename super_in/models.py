from django.contrib.auth.models import User
from django.db import models
from django.db.models.fields import CharField
from django.contrib.auth.models import User

#-------------------------------------------------------------------------
#  Company Model
#-------------------------------------------------------------------------
class Users(models.Model):
    user_id=models.IntegerField(primary_key=True)
    name=models.CharField(max_length=100)
    company=models.CharField(max_length=50)
    email=models.CharField(max_length=50)
    role=models.CharField(max_length=20)
    password=models.CharField(max_length=10)
    company_id=models.IntegerField()
    role_id=models.IntegerField()
    
    
##############################################################
#Ship model
##############################################################
class Ship(models.Model):
    id=models.IntegerField(primary_key=True)
    company_id=models.IntegerField()
    name = models.CharField(max_length=20)
    type = models.CharField(max_length=20)
    fleet_manager=models.CharField(max_length=20)
    superintendent=models.CharField(max_length=20)

    
##############################################################
#Categories model
##############################################################
class Category(models.Model):
    id=models.IntegerField(primary_key=True)
    category_name = models.CharField(max_length=20)
    ship_type = models.CharField(max_length=20)



##############################################################
#Vessel_type model
##############################################################
class Vessel_type(models.Model):
    vt_id=models.IntegerField(primary_key=True)
    vessel_type = models.CharField(max_length=20)


##############################################################
#Template_categories model
##############################################################
class Template_categories(models.Model):
    tc_id=models.IntegerField(primary_key=True)
    tc_name = models.CharField(max_length=20)
    vt_id=models.IntegerField()

##############################################################
#Template_question_categories model
##############################################################
class Question_categories(models.Model):
    qc_id=models.IntegerField(primary_key=True)
    ship_type = models.CharField(max_length=20)
    category_name=models.CharField(max_length=20)
    company=models.CharField(max_length=50)
    company_id=models.IntegerField()

##############################################################
#Template_question_categories model
##############################################################
class Template_Questions(models.Model):
    tq_id=models.IntegerField(primary_key=True)
    template_question = models.CharField(max_length=50)
    tc_name=models.CharField(max_length=50)


##############################################################
#company detail  model
##############################################################
 
class Company_Details(models.Model):
    id=models.IntegerField(primary_key=True)
    company_name = models.CharField(max_length=50)
    company_address=models.CharField(max_length=50)   



##############################################################
#role detail  model
##############################################################
 
class Roles(models.Model):
    role_id=models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    
##############################################################
#company template relation model
##############################################################
 
class Template_Company(models.Model):
    id=models.IntegerField(primary_key=True)
    company_id=models.IntegerField()
    template_id=models.IntegerField()
    

##############################################################
#checklist Questions model
##############################################################
 
class Checklist_Questions(models.Model):
    cq_id=models.IntegerField(primary_key=True)
    cc_id=models.IntegerField()
    tq_id=models.CharField(max_length=500000)
    