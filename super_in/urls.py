from django.conf.urls import patterns, include, url
from tastypie.api import Api
from django.contrib import admin
from api.api import UsersResource,ShipsResource,QuestionsResource,TemplatesResource
from super_in import views

admin.autodiscover()
users_resource = UsersResource()
ships_resource = ShipsResource()
questions_resource = QuestionsResource()
templates_resource = TemplatesResource()
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'delico.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(users_resource.urls)),
    url(r'^$', 'super_in.views.home', name='home'),
    url(r'^api/users/(?P<name>.+)/$', views.UsersView.as_view(), name='users-list'),
    url(r'^api/users/$', 'super_in.views.UsersViews', name='users-list'),
    url(r'^api/ships/$', views.ShipsView.as_view(), name='ships-list'),
    url(r'^api/questions/$', views.QuestionsView.as_view(), name='questions-list'),
    url(r'^api/templates/$', views.TemplatesView.as_view(), name='templates-list'),
    url(r'^signUp/$', 'super_in.views.signup', name='signup'),
    url(r'^login/$', 'super_in.views.login', name='login'),
    url(r'^deleteUser/$', 'super_in.views.deleteUser', name='deleteUser'),
    url(r'^editUser/$', 'super_in.views.editUser', name='editUser'),
    url(r'^getUsers/$', 'super_in.views.getUsersData', name='getUsersData'),
    url(r'^autocomplete/$','super_in.views.getvalues', name='getvalues'),
    url(r'^autocomplete_managers/$','super_in.views.getManagers', name='getManagers'),
    url(r'^autocomplete_superintendents/$','super_in.views.getSuperintendents', name='getSuperintendents'),
    url(r'^saveUser/$', 'super_in.views.saveUser', name='saveUser'),
    url(r'^getShips/$', 'super_in.views.getShipsData', name='getShipsData'),
    url(r'^saveShip/$', 'super_in.views.saveShip', name='saveShip'),
    url(r'^deleteShip/$', 'super_in.views.deleteShip', name='deleteShip'),
    url(r'^editShip/$', 'super_in.views.editShip', name='editShip'),
    url(r'^getShipTypes/$', 'super_in.views.getShips_Types', name='getShips_Types'),
    url(r'^getVesselTypes/$', 'super_in.views.getVessel_Types', name='getVessel_Types'),
    url(r'^getShipTypes_1/$', 'super_in.views.getShips_Types_1', name='getShips_Types_1'),
    url(r'^getCategories/$', 'super_in.views.getCategories', name='getCategories'),
    url(r'^saveCategory/$', 'super_in.views.saveCategory', name='saveCategory'),
    url(r'^saveTemplate/$', 'super_in.views.saveTemplate', name='saveTemplate'),
    url(r'^getTemplates/$', 'super_in.views.getTemplates', name='getTemplates'),
    url(r'^saveSelectedQuestions/$', 'super_in.views.saveSelectedQuestions', name='saveSelectedQuestions'),
    url(r'^getLoggedInUser/$', 'super_in.views.getLoggedInUser', name='getLoggedInUser'),
    #url(r'^templateQuestion/$', 'super_in.views.templateQuestion', name='templateQuestion'),
    url(r'^getCategoryQuestions/$', 'super_in.views.getCategoryQuestions', name='getCategoryQuestions'),
    url(r'^saveQuestion/$', 'super_in.views.saveQuestion', name='saveQuestion'),
    url(r'^logout/$', 'super_in.views.logout', name='logout'),
    url(r'^saveEditedTemplate/$', 'super_in.views.saveEditedTemplate', name='saveEditedTemplate'),
    url(r'^sign-up/$', 'super_in.views.usersignup', name='usersignup'),
    
    )