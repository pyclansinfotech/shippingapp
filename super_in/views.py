from django.shortcuts import render
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.template import Context
from django.views.decorators.csrf import csrf_exempt
from models import Users,Ship,Vessel_type,Template_categories,Question_categories,Template_Questions,Company_Details,Roles,Template_Company,Checklist_Questions
from django.http import HttpResponse
from django.http import JsonResponse
import json
from django.http import HttpResponseRedirect
from serializers import UsersSerializer,ShipsSerializer,QuestionsSerializer,TemplatesSerializer
from rest_framework import generics
from rest_framework.permissions import IsAdminUser
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
import base64
import sys
import ast

###render to home page###
def home(request):
	ctx = RequestContext(request, {})
	return render_to_response('login.html',
                              {
                              }, context_instance=ctx)

###render to home page###
def usersignup(request):
	ctx = RequestContext(request, {})
	return render_to_response('register.html',
                              {
                              }, context_instance=ctx)

@csrf_exempt
def getUsersData(request):
	user_company=request.session['company']
	user_email=request.session['user']
	print 'company',user_company
	users = Users.objects.filter(company=user_company).exclude(email=user_email)
	results = []
	for user in users:
		user_json = {}
		user_json['id'] = user.user_id
		user_json['name'] = user.name
		user_json['company'] = user.company
		user_json['email'] = user.email
		user_json['role'] = user.role
		results.append(user_json)
		data = json.dumps(results)
		mimetype = 'application/json'
	return HttpResponse(data)


### get ship data###
@csrf_exempt
def getShipsData(request):
	company_name=request.session['company'].replace(' ','')
	print 'role get',company_name,len(company_name)
	companies = Company_Details.objects.filter(company_name=company_name)
	if companies:
		for company in companies:
			c_id=company.id
			print 'company_id get', c_id
	else:
		print 'error'
	ships = Ship.objects.filter(company_id=c_id)
	results = []
	for ship in ships:
		ship_json = {}
		ship_json['id'] = ship.id
		ship_json['name'] = ship.name
		ship_json['type'] = ship.type
		ship_json['fleetmanager'] = ship.fleet_manager
		ship_json['superintendent'] = ship.superintendent
		results.append(ship_json)
		data = json.dumps(results)
		mimetype = 'application/json'
	return HttpResponse(data)

### Delete a user ###
@csrf_exempt
def deleteUser(request):
	req_string=str(request.body)
	print 'request--',req_string
	req=req_string.split('=')
	user_id=req[1]
	print 'user_id',user_id
	Users.objects.filter(user_id=user_id).delete()
	return HttpResponse('success')


### Edit a user ###
@csrf_exempt
def editUser(request):
	req_string=str(request.body)
	req=req_string.split('&')
	user_id_str=req[0].split('=')
	user_id=user_id_str[1]
	user_name_string=req[1].split('=')
	user_name=user_name_string[1].replace('+','').title()
	user_email_string=req[2].split('=')
	user_email=user_email_string[1].replace('%40','@')
	user_company_string=req[3].split('=')
	user_company=user_company_string[1].replace('+','')
	user_role_string=req[4].split('=')
	user_role=user_role_string[1].replace('+',' ')
	r = Roles.objects.filter(name=user_role)
	for a in r:
		role_id=a.role_id
		print 'role_id get', role_id
	user=Users.objects.get(user_id=user_id)
	user.name=user_name
	user.email=user_email
	user.company=user_company
	user.role=user_role
	user.role_id=role_id
	user.save()
	return HttpResponse('success')

### save user  ###
@csrf_exempt
def saveUser(request):
	list_em=[]
	list_id=[]
	req_string=str(request.body)
	print 'req_string>>>',req_string
	req=req_string.split('&')
	pass_str=req[0].split('=')
	password=pass_str[1]
	new_password=base64.b64encode(password)
	name_str=req[1].split('=')
	name=name_str[1].replace('+','')
	name1=name.title()
	email_str=req[2].split('=')
	email=email_str[1].replace('%40','@')
	company=request.session['company']
	role_str=req[3].split('=')
	role=role_str[1].replace('+',' ')
	company_name=request.session['company'].replace(' ','')
	print 'role get',role,len(company_name)
	r = Roles.objects.filter(name=role)
	for a in r:
		role_id=a.role_id
		print 'role_id get', role_id
	
	companies = Company_Details.objects.filter(company_name=company_name)
	print 'company',companies
	if companies:
		for company in companies:
			c_id=company.id
			print 'company_id get', c_id
	else:
		print 'error'
	try:
		for user in Users.objects.raw('SELECT * FROM super_in_users'):
			em=user.email
			id_test=user.user_id
			list_em.append(em)
			list_id.append(id_test)
		var_num=max(list_id)
	except Exception as e:
		var_num=1
	user_email=email
	if user_email in list_em:
		return HttpResponse('already exist')
	else:
		user_new_id=var_num+1
		user = Users.objects.create(
				user_id=user_new_id,
	            name = name1,
	            email = email,
	            password = new_password,
	            role = role,
	            company=company_name,
	            company_id=c_id,
	            role_id=role_id,)
		user.save()
		return HttpResponse('success')


### save Ship info  ###
@csrf_exempt
def saveShip(request):
	list_id=[]
	req_string=str(request.body)
	print 'data',req_string
	user_company=request.session['company']
	comp=user_company.replace(' ','')
	print 'company',comp,len(comp)
	comp = Company_Details.objects.filter(company_name=comp)
	for c in comp:
		comp_id=c.id
	print 'comp_id>>',comp_id
	req=req_string.split('&')
	type_str=req[0].split('=')
	ship_type=type_str[1].replace('+',' ')
	name_str=req[1].split('=')
	ship_name=name_str[1]
	manager_str=req[2].split('=')
	ship_manager=manager_str[1].replace('+','')
	superintendent_str=req[3].split('=')
	ship_superintendent=superintendent_str[1].replace('+','')
	try:
		for ship in Ship.objects.raw('SELECT * FROM super_in_ship'):
			id_test=ship.id
			list_id.append(id_test)
		num_max=max(list_id)
		var_num=num_max+1
	except Exception as e:
		var_num=1
	ship = Ship.objects.create(
				id = var_num,
	            name = ship_name,
	            type = ship_type,
	            fleet_manager = ship_manager,
	            superintendent = ship_superintendent,
	            company_id=comp_id,
	     		)
	ship.save()
	return HttpResponse('success')


### Delete a ship ###
@csrf_exempt
def deleteShip(request):
	req_string=str(request.body)
	print 'request--',req_string
	req=req_string.split('=')
	ship_id=req[1]
	print 'ship_id',ship_id
	Ship.objects.filter(id=ship_id).delete()
	return HttpResponse('success')

### Edit a Ship ###
@csrf_exempt
def editShip(request):
	req_string=str(request.body)
	req=req_string.split('&')
	print 'req>>',req
	ship_id_str=req[0].split('=')
	ship_id=ship_id_str[1]
	ship_name_string=req[1].split('=')
	ship_name=ship_name_string[1].replace('+','')
	ship_type_string=req[2].split('=')
	ship_type=ship_type_string[1].replace('+','')
	ship_fleetmanager_string=req[3].split('=')
	ship_manager=ship_fleetmanager_string[1].replace('+','')
	ship_superintendent_string=req[4].split('=')
	ship_superintendent=ship_superintendent_string[1].replace('+','')
	print 'data>>',ship_manager,ship_superintendent,ship_id,ship_name,ship_type
	ship=Ship.objects.get(id=ship_id)
	ship.name=ship_name
	ship.type=ship_type
	ship.fleet_manager=ship_manager
	ship.superintendent=ship_superintendent
	ship.save()
	return HttpResponse('success')

		
#saveEditedTemplate
@csrf_exempt
def saveEditedTemplate(request):
	req_string=request.body
	print 'req_string',req_string
	req=req_string.split('&')
	print 'req>>',req
	list_len=len(req)
	template_id_str=req[0].split('=')
	template_id=template_id_str[1]
	print 'template_id>>',template_id
	categories=[]
	for i in range(1,list_len):
		category_name_list=req[i].split('=')
		category_name=category_name_list[1].replace('+',' ')
		categoryname=category_name.replace('%26','&')
		categories.append(categoryname)
	print 'categories>>>>',categories
	template=Question_categories.objects.get(qc_id=template_id)
	template.category_name=categories
	template.save()
	return HttpResponse('success')






@csrf_exempt
def login(request):
	try:
		req_string=request.POST
		username=request.POST.get('email')
		password=request.POST.get('password')
		print 'username>>',username,password
		new_password=base64.b64encode(password)
		print username,new_password
		data=Users.objects.filter(email=username).filter(role='Admin')
		print 'data',data,type(data)
		if data:
			for p in data:
				user = p.email
				company=p.company
				password=p.password
				password_new=password.replace(' ','')
				print 'password_new>>', password_new,new_password
				if(new_password==password_new):
					request.session['user']=user
					request.session['company']=company
					ctx = RequestContext(request, {})
					return render_to_response('index.html',
			                              {
			                              }, context_instance=ctx)
				else:
					return HttpResponseRedirect("/"+ "?user=no")
		else:
			return HttpResponseRedirect("/"+ "?user=no")
	except Exception as e:
		print e
	


###get company names for autocomplete###
def getvalues(request):
	starts=request.GET['term']
	start_alphabet=starts.upper()
	print 'start_alphabet',start_alphabet
	users = Company_Details.objects.filter(company_name__contains=start_alphabet)
	results = []
	for user in users:
		user_json = {}
		user_json['id'] = user.id
		user_json['label'] = user.company_name
		user_json['value'] = user.company_name
		results.append(user_json)
		data = json.dumps(results)
		mimetype = 'application/json'
	return HttpResponse(data, mimetype)

###get fleet_manager names for autocomplete###
def getManagers(request):
	starts=request.GET['term']
	start_alphabet=starts.title()
	user_company=request.session['company']
	comp=user_company.replace(' ','')
	users = Users.objects.filter(role='Fleet Manager').filter(company=comp).filter(name__contains=start_alphabet)
	results = []
	for user in users:
		user_json = {}
		user_json['id'] = user.user_id
		user_json['label'] = user.name
		user_json['value'] = user.name
		results.append(user_json)
		data = json.dumps(results)
		mimetype = 'application/json'
	return HttpResponse(data, mimetype)


###get ship types for autocomplete###
def getVessel_Types(request):
	starts=request.GET['term']
	start_alphabet=starts.title()
	users = Vessel_type.objects.all()
	results = []
	for user in users:
		user_json = {}
		user_json['id'] = user.vt_id
		user_json['label'] = user.vessel_type
		user_json['value'] = user.vessel_type
		results.append(user_json)
		data = json.dumps(results)
		mimetype = 'application/json'
	return HttpResponse(data, mimetype)

###get Superintendent names for autocomplete###
def getSuperintendents(request):
	starts=request.GET['term']
	start_alphabet=starts.title()
	user_company=request.session['company']
	comp=user_company.replace(' ','')
	users = Users.objects.filter(role='Superintendent').filter(company=comp).filter(name__contains=start_alphabet)
	results = []
	for user in users:
		user_json = {}
		user_json['id'] = user.user_id
		user_json['label'] = user.name
		user_json['value'] = user.name
		results.append(user_json)
		data = json.dumps(results)
		mimetype = 'application/json'
	return HttpResponse(data, mimetype)



###Function for signup ###
@csrf_exempt
def signup(request):
	print 'request>>>>',request.POST
	role=request.POST['id-role']
	company_name=request.POST['id-company']
	print 'role get',role,company_name
	roles = Roles.objects.filter(name=role)
	for role in roles:
		role_id=role.role_id
		print 'role_id get', role_id
	
	companies = Company_Details.objects.filter(company_name=company_name)
	if companies:
		for company in companies:
			c_id=company.id
			print 'company_id get', c_id

	else:
		list_id=[]
		companies = Company_Details.objects.all()
		for company in companies:
			company_id=company.id
			list_id.append(company_id)
			vars_num=max(list_id)
			c_id=vars_num+1
		company = Company_Details.objects.create(
			id=vars_num+1,
			company_name=company_name,
			)
		company.save()
	list_em=[]
	list_id=[]
	#print 'request>>>',request.POST['id-company']
	try:
		for user in Users.objects.raw('SELECT * FROM super_in_users'):
			em=user.email
			list_em.append(em)
			ids=user.user_id
			list_id.append(ids)
		print 'list_em>>',list_em
		var_num=max(list_id)
		print 'var_num',var_num
	except Exception as e:
		var_num=1
	user_email=request.POST['id-email']
	if user_email in list_em:
		return HttpResponse('already exist')
	else:
		user_new_id=var_num+1
		user_name=request.POST['id-name']
		username=user_name.title()
		email=request.POST['id-email']
		password=request.POST['id-password']
		new_password=base64.b64encode(password)
		role=request.POST['id-role']
		company=request.POST['id-company']
		user = Users.objects.create(
				user_id=user_new_id,
	            name = username,
	            email = email,
	            password = new_password,
	            role = role,
	            company=company,
	            company_id=c_id,
	            role_id=role_id)
		user.save()
		return HttpResponse('success')


### Get ship types  ###
@csrf_exempt
def getShips_Types(request):
	req_data=request.body.split('&')
	print 'req_data',req_data
	results = []
	arr=[]
	for i in range(0,len(req_data)):
		name=req_data[i].split('=')
		name_type=name[1].replace('+',' ')
		print 'name',name_type
		arr.append(name_type)
		print 'arr',arr
	blocked = Question_categories.objects.all().values_list('ship_type')
	vessel_data = Vessel_type.objects.exclude(vessel_type__in=blocked)
	print 'vessel_data',vessel_data
	for data in vessel_data:
		print 'vessel_data',data.vessel_type
		ship_json = {}
		ship_json['id'] = data.vt_id
		ship_json['type'] = data.vessel_type
		results.append(ship_json)
		data = json.dumps(results)
		mimetype = 'application/json'
	return HttpResponse(data, mimetype)



### Get ship types  ###
@csrf_exempt
def getShips_Types_1(request):
	results = []
	vessel_data = Vessel_type.objects.all()
	for data in vessel_data:
		ship_json = {}
		ship_json['id'] = data.vt_id
		ship_json['type'] = data.vessel_type
		results.append(ship_json)
		data = json.dumps(results)
		mimetype = 'application/json'
	return HttpResponse(data, mimetype)


### Get categories for a particular ship type ###
@csrf_exempt
def getCategories(request):
	req_string=request.body
	print 'req_string',req_string
	data_str=req_string.split('=')
	ship_type=data_str[1]
	print 'ship>>',ship_type,type(ship_type)
	results = []
	if ship_type.isdigit():
		categories = Template_categories.objects.filter(vt_id=ship_type)
	else:
		shiptypetstr=ship_type.split("+",1)
		s_new=shiptypetstr[0]+' '+shiptypetstr[1]
		Shiptype_new=s_new.replace('+','')
		shiptype_name=Shiptype_new.rstrip()
		vessel_data = Vessel_type.objects.filter(vessel_type=shiptype_name)
		for v_data in vessel_data:
			shiptype=v_data.vt_id
		categories = Template_categories.objects.filter(vt_id= shiptype)
	for category in categories:
		category_json = {}
		category_json['id'] = category.tc_id
		category_json['name'] = category.tc_name
		results.append(category_json)
		data = json.dumps(results)
		print 'data?>>>',data
		mimetype = 'application/json'
	return HttpResponse(data, mimetype)



### Get question categories with ship type ###
@csrf_exempt
def getQuestionCategories(request):
	results = []
	question_categories = Question_categories.objects.all()
	for data in question_categories:
		cat_json = {}
		cat_json['id'] = data.qc_id
		cat_json['ship_type'] = data.ship_type
		cat_json['category_name'] = data.category_name
		results.append(cat_json)
		data = json.dumps(results)
		mimetype = 'application/json'
	return HttpResponse(data, mimetype)



### save new category for a particular ship type ###
@csrf_exempt
def saveCategory(request):
	req_string=request.body
	print 'req>>',request.body
	data_str=req_string.split('&')
	print 'data_str>>>>',data_str
	id_str=data_str[0].split('=')
	ship_type_id=id_str[1]
	name_str=data_str[1].split('=')
	name=name_str[1].replace('+',' ')
	print 'name',name
	results = []
	list_id=[]
	for category in Template_categories.objects.raw('SELECT * FROM super_in_template_categories'):
		cat_id=category.tc_id
		list_id.append(cat_id)
		num_max=max(list_id)
		tc_id=num_max+1
	print 'cat>>>>',cat_id,tc_id,ship_type_id
	category = Template_categories.objects.create(
				tc_id=int(tc_id),
				vt_id=ship_type_id,
	            tc_name = name)
	category.save()
	categories = Template_categories.objects.filter(vt_id=ship_type_id)
	for category in categories:
		category_json = {}
		category_json['id'] = category.tc_id
		category_json['name'] = category.tc_name
		results.append(category_json)
		data = json.dumps(results)
		mimetype = 'application/json'
	return HttpResponse(data, mimetype)

@csrf_exempt
def getTemplates(request):
	user_company=request.session['company']
	#print 'inside get templates'
	results = []
	question_categories = Question_categories.objects.filter(company=user_company)
	if question_categories:
		for data in question_categories:
			cat_json = {}
			cat_json['id'] = data.qc_id
			cat_json['ship_type'] = data.ship_type
			cat_json['category_name'] = data.category_name
			cat_json['company'] = data.company
			results.append(cat_json)
			data = json.dumps(results)
			mimetype = 'application/json'
		return HttpResponse(data, mimetype)
	else:
		return HttpResponse('nodata')


### Save template for ship type ###
@csrf_exempt
def saveTemplate(request):
	req_string=request.body
	print 'data>',req_string
	data_str=req_string.split('&')
	print 'data_str',data_str,len(data_str)
	list_len=len(data_str)
	id_str=data_str[0].split('=')
	ship_typeid=id_str[1]
	ships = Vessel_type.objects.filter(vt_id=ship_typeid)
	for ship in ships:
		ship_type=ship.vessel_type
		print 'ship_type',ship_type
	for category in Question_categories.objects.raw('SELECT * FROM super_in_question_categories'):
		print 'yes'
		print 'category',category
		list_id=[]
		cat_id=category.qc_id
		list_id.append(cat_id)
		num_max=max(list_id)
		print 'num_max',num_max
		user_company=request.session['company']
		company_name=user_company.replace(' ','')
	comp = Company_Details.objects.filter(company_name=company_name)
	for c in comp:
		c_id=c.id
	categories=[]
	for i in range(1,list_len):
		ques_cat_id_new=num_max+i
		category_name_list=data_str[i].split('=')
		category_name=category_name_list[1].replace('+',' ')
		categoryname=category_name.replace('%26','&')
		categories.append(categoryname)
	print 'categories>>>>',categories
	question_category = Question_categories.objects.create(
				qc_id=ques_cat_id_new,
				ship_type=ship_type,
	            category_name = categories,
	            company = user_company,
	            company_id=c_id,)
	question_category.save()
	results = []
	question_categories = Question_categories.objects.filter(company=user_company)
	for data in question_categories:
		cat_json = {}
		cat_json['id'] = data.qc_id
		cat_json['ship_type'] = data.ship_type
		cat_json['category_name'] = data.category_name
		cat_json['company'] = data.company
		results.append(cat_json)
		data = json.dumps(results)
		mimetype = 'application/json'
	return HttpResponse(data, mimetype)
		

### Get question ###
@csrf_exempt
def getCategoryQuestions(request):
	req_string=request.body
	print 'data>',req_string
	category_str=req_string.split('=')
	category=category_str[1].replace('+',' ')
	cat=category.replace('%26','&')
	print 'data_category>',cat
	results = []
	category_data=Template_categories.objects.filter(tc_name=cat)[0]
	category_id=category_data.tc_id
	print 'id>>>',category_id
	saved_ques=Checklist_Questions.objects.filter(cc_id=category_id)
	for data in saved_ques:
		saved_questions_id=data.tq_id
		n = ast.literal_eval(saved_questions_id)
	print 'saved_questions_id',saved_questions_id,n,type(n)
	question_data = Template_Questions.objects.filter(tc_name=cat)
	if question_data:
		print 'question_data',question_data
		for question in question_data:
			qcdata_json = {}
			qcdata_json['id'] = question.tq_id
			qcdata_json['template_question'] = question.template_question
			qcdata_json['tc_name'] = question.tc_name
			results.append(qcdata_json)
			print 'results>>',results
			for a in n:
				print 'aa>',a,qcdata_json['id']
				if qcdata_json['id'] == a:
					qcdata_json['check'] = 'true'
				else:
					pass
			data = json.dumps(results)
			print 'data>>',data
			mimetype = 'application/json'
		return HttpResponse(data, mimetype)
	else:
		qcdata_json = {}
		qcdata_json['id'] = ''
		qcdata_json['template_question'] = ''
		qcdata_json['tc_name'] = ''
		results.append(qcdata_json)
		data = json.dumps(results)
		mimetype = 'application/json'
	return HttpResponse(data, mimetype)

### Save a new  question ###
@csrf_exempt
def saveQuestion(request):
	req_string=request.body
	print 'data_questions>',req_string
	data_str=req_string.split('&')
	category_data_str=data_str[0]
	category_name=category_data_str.replace('+%26+',' & ')
	cat_str=category_name.split('=')
	cat_name=cat_str[1]
	print 'cat_name',cat_name
	category_data=Template_categories.objects.filter(tc_name=cat_name)[0]
	category_id=category_data.tc_id
	print 'id>>>',category_id
	saved_ques=Checklist_Questions.objects.filter(cc_id=category_id)
	for data in saved_ques:
		saved_questions_id=data.tq_id
		n = ast.literal_eval(saved_questions_id)
	print 'saved_questions_id',saved_questions_id,n,type(n)
	question_data = Template_Questions.objects.all()
	for category in question_data:
		print 'category>>>>',category.template_question
		list_id=[]
		cat_id=category.tq_id
		list_id.append(cat_id)
		print 'list_id',list_id
		num_max=max(list_id)
		print 'num_max',num_max
		tq_id=num_max+1
	req_string=request.body
	print 'data>',req_string
	data_str=req_string.split('&')
	category_str=data_str[0].split('=')
	category_name=category_str[1].replace('+','')
	category_n=category_name.replace('%26',' & ')
	question_str=data_str[1].split('=')
	question=question_str[1].replace('+',' ')
	question_new=question.replace('%3F','?')
	question_new1=question_new.replace('%2C',',')
	question_n=question_new1.replace('%26',' & ')
	print 'vars',category_n,question_n
	question_category = Template_Questions.objects.create(
				tq_id=tq_id,
				template_question=question_n,
	            tc_name = category_n)
	question_category.save()
	results = []
	question_datas = Template_Questions.objects.filter(tc_name=category_n)
	if question_datas:
		print 'question_data',question_datas
		for question in question_datas:
			qcdata_json = {}
			qcdata_json['id'] = question.tq_id
			qcdata_json['template_question'] = question.template_question
			qcdata_json['tc_name'] = question.tc_name
			results.append(qcdata_json)
			for a in n:
				print 'aa>',a,qcdata_json['id']
				if qcdata_json['id'] == a:
					qcdata_json['check'] = 'true'
				else:
					pass
			data = json.dumps(results)
			mimetype = 'application/json'
		return HttpResponse(data, mimetype)
	else:
		print 'error'''


### Save selected Questions ###
@csrf_exempt
def saveSelectedQuestions(request):
	req_string=request.body
	print 'data_questions>',req_string
	data_str=req_string.split('&')
	print 'data_str',data_str,len(data_str)
	length=len(data_str)
	idx2=len(data_str)-2
	idx1=len(data_str)-1
	temp=data_str[idx2].split('=')
	temp_id=temp[1]
	category=data_str[idx1].split('=')
	category_name=category[1].replace('+',' ')
	category_n=category_name.replace('%26','&')
	print 'category>>',category_n
	company_name=request.session['company'].replace(' ','')
	companies = Template_categories.objects.filter(tc_name=category_n)[0]
	print 'company',companies.tc_id
	if companies:
			c_id=companies.tc_id
	else:
		print 'error'
	print 'template_id=,compny_id',temp_id,c_id
	checklist_data=Checklist_Questions.objects.filter(cc_id=c_id)
	if checklist_data:
		print 'yes'
		for c in checklist_data:
			question_id_list=c.tq_id
			print 'question_id_list',question_id_list
	question_ids=[]
	list_id=[]
	data=Checklist_Questions.objects.all()
	print 'data>>',data
	if data:
		for d in data:
			id_test=d.cq_id
			list_id.append(id_test)
			num_max=max(list_id)
			var_num=num_max+1
	else:
		var_num=1
	for i in range(0,idx1):
		ques=data_str[i].split('=')
		quest=ques[1].replace('+',' ')
		question=quest.replace('%2C',',')
		question_final=question.replace('%3F','?')
		print 'question_final>>>',question_final
		question_data = Template_Questions.objects.filter(template_question=question_final)
		for data in question_data:
			question_id=data.tq_id
			print 'question_id',question_id
			question_ids.append(question_id)
			print 'question_ids in',question_ids
		print 'question_ids out',question_ids,question_id_list
		n=ast.literal_eval(question_id_list)
		for q in n:
			question_ids.append(q)
		ques_ids=list(set(question_ids))
		print 'new question_id>>>s',ques_ids
	selected_questions = Checklist_Questions.objects.create(
				cq_id = var_num,
				cc_id = c_id,
	            tq_id = ques_ids)
	selected_questions.save()
	return HttpResponse('success')

### Get logged in user data ###
@csrf_exempt
def getLoggedInUser(request):
	user_company=request.session['company'].replace(' ','')
	user_email=request.session['user']
	users = Users.objects.filter(email=user_email)
	for user in users:
		user_name=user.name
	print 'user>>>',user_name,user_company
	results=[]
	data_json = {}
	data_json['name'] = user_name.replace(' ','')
	data_json['company'] = user_company
	results.append(data_json)
	data = json.dumps(results)
	mimetype = 'application/json'
	return HttpResponse(data, mimetype)


def logout(request):
	print 'logout'
	request.session['user']=''
	request.session['company']=''
	ctx = RequestContext(request, {})
	return HttpResponseRedirect("/")
	
###########################################API VIEWS#############################################

### API FOR SIGN UP AND USERS LIST
class UsersView(generics.ListCreateAPIView):
    serializer_class = UsersSerializer
    def get_queryset(self):
	    name = self.kwargs['name']
	    queryset = Users.objects.filter(name=name)
	    model = Users
	    return queryset


### API FOR SIGN UP AND USERS LIST
#class UsersViews(generics.ListCreateAPIView):
@api_view(['GET', 'POST', ])
def UsersViews(request, format=None):
    content = {'please provide a name along with users api': 'nothing to see here'}
    return Response(content, status=status.HTTP_401_UNAUTHORIZED)

### API FOR SHIPS
class ShipsView(generics.ListCreateAPIView):
    """
    Returns a list of all ships.
    """
    queryset = Ship.objects.all()
    model = Ship
    serializer_class = ShipsSerializer
    


### API FOR Questions
class QuestionsView(generics.ListCreateAPIView):
    """
    Returns a list of all questions.
    """
    queryset = Template_Questions.objects.all()
    model = Template_Questions
    serializer_class = QuestionsSerializer


### API FOR Templates
class TemplatesView(generics.ListCreateAPIView):
    """
    Returns a list of all questions.
    """
    queryset = Question_categories.objects.all()
    model = Question_categories
    serializer_class = TemplatesSerializer